//
//  YZFullscreenPopGesture.m
//  YZBackGRProject
//
//  Created by  on 2019/7/17.
//  Copyright © 2019 . All rights reserved.
//

#import "YZFullscreenPopGesture.h"
#import <objc/message.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface YZFullscreenPopGestureDelegate : NSObject<UIGestureRecognizerDelegate>
+ (instancetype)shared;
@end

@implementation YZFullscreenPopGestureDelegate
+ (instancetype)shared {
    static YZFullscreenPopGestureDelegate *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = YZFullscreenPopGestureDelegate.alloc.init;
    });
    return instance;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    UINavigationController *_Nullable nav = [self _lookupResponder:gestureRecognizer.view class:UINavigationController.class];
    
    if ( nav == nil )
        return false;
    
    if ( nav.childViewControllers.count <= 1 )
        return false;
    
    if ( [[nav valueForKey:@"isTransitioning"] boolValue] )
        return false;
    
    if ( nav.topViewController.YZ_disableFullscreenGesture )
        return false;
    
    if ( [self _blindAreaContains:nav point:[touch locationInView:nav.view]] )
        return false;
    
    if ( [nav.childViewControllers.lastObject isKindOfClass:UINavigationController.class] )
        return false;
    
    if ( nav.topViewController.YZ_considerWebView )
        return !nav.topViewController.YZ_considerWebView.canGoBack;
    
    return true;
}

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer {
    if ( YZFullscreenPopGesture.gestureType == YZFullscreenPopGestureTypeEdgeLeft )
        return true;
    
    CGPoint translate = [gestureRecognizer translationInView:gestureRecognizer.view];
    
    if ( translate.x > 0 && translate.y == 0 )
        return true;
    
    return false;
}

- (BOOL)gestureRecognizer:(UIPanGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    if ( gestureRecognizer.state == UIGestureRecognizerStateFailed ||
         gestureRecognizer.state == UIGestureRecognizerStateCancelled )
        return false;
    
    if ( YZFullscreenPopGesture.gestureType == YZFullscreenPopGestureTypeEdgeLeft ) {
        [self _cancelGesture:otherGestureRecognizer];
        return true;
    }

    UINavigationController *nav = [self _lookupResponder:gestureRecognizer.view class:UINavigationController.class];

    CGPoint location = [gestureRecognizer locationInView:gestureRecognizer.view];
    
    if ( [self _blindAreaContains:nav point:location] )
        return false;
    
    if ( [otherGestureRecognizer isMemberOfClass:NSClassFromString(@"UIScrollViewPanGestureRecognizer")] ||
         [otherGestureRecognizer isMemberOfClass:NSClassFromString(@"UIScrollViewPagingSwipeGestureRecognizer")] ) {
        if ( [otherGestureRecognizer.view isKindOfClass:UIScrollView.class] ) {
            return [self _shouldRecognizeSimultaneously:(id)otherGestureRecognizer.view gestureRecognizer:gestureRecognizer otherGestureRecognizer:otherGestureRecognizer];
        }
    }
    
    if ( [otherGestureRecognizer.view isKindOfClass:NSClassFromString(@"_MKMapContentView")] ||
         [otherGestureRecognizer isKindOfClass:NSClassFromString(@"UIWebTouchEventsGestureRecognizer")] ) {
        if ( [self _edgeAreaContains:nav point:location] ) {
            [self _cancelGesture:otherGestureRecognizer];
            return true;
        }
        else
            return false;
    }

    if ( [otherGestureRecognizer isKindOfClass:UIPanGestureRecognizer.class] )
        return false;

    return false;
}

- (BOOL)_shouldRecognizeSimultaneously:(UIScrollView *)scrollView gestureRecognizer:(UIPanGestureRecognizer *)gestureRecognizer otherGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {

    if ( [scrollView isKindOfClass:NSClassFromString(@"_UIQueuingScrollView")] ) {
        if ( scrollView.isDecelerating )
            return false;
        
        UIPageViewController *pageVC = [self _lookupResponder:scrollView class:UIPageViewController.class];
        
        if ( pageVC.viewControllers.count == 0 )
            return false;
        
        if ( [pageVC.dataSource pageViewController:pageVC viewControllerBeforeViewController:pageVC.viewControllers.firstObject] != nil )
            return false;
        
        [self _cancelGesture:otherGestureRecognizer];
        
        return true;
    }
    
    CGPoint translate = [gestureRecognizer translationInView:gestureRecognizer.view];
    
    if ( 0 == scrollView.contentOffset.x + scrollView.contentInset.left
        && !scrollView.isDecelerating
        && translate.x > 0 && 0 == translate.y ) {
        [self _cancelGesture:otherGestureRecognizer];
        return true;
    }
    
    return false;
}

// --

- (nullable __kindof UIResponder *)_lookupResponder:(UIView *)view class:(Class)cls {
    __kindof UIResponder *_Nullable next = view.nextResponder;
    while ( next != nil && [next isKindOfClass:cls] == NO ) {
        next = next.nextResponder;
    }
    return next;
}

- (void)_cancelGesture:(UIGestureRecognizer *)gesture {
    [gesture setValue:@(UIGestureRecognizerStateCancelled) forKey:@"state"];
}

- (BOOL)_edgeAreaContains:(UINavigationController *)nav point:(CGPoint)point {
    CGFloat offset = 50;
    CGRect rect = CGRectMake(0, 0, offset, nav.view.bounds.size.height);
    
    return [self _rectContains:nav rect:rect point:point shouldConvertRect:NO];
}

- (BOOL)_blindAreaContains:(UINavigationController *)nav point:(CGPoint)point {
    for ( NSValue *rect in nav.topViewController.YZ_blindArea ) {
        if ( [self _rectContains:nav rect:[rect CGRectValue] point:point shouldConvertRect:YES] )
            return true;
    }
    
    for ( UIView *view in nav.topViewController.YZ_blindAreaViews ) {
        if ( [self _rectContains:nav rect:[view frame] point:point shouldConvertRect:YES] )
            return true;
    }
    
    return false;
}

- (BOOL)_rectContains:(UINavigationController *)nav rect:(CGRect)rect point:(CGPoint)point shouldConvertRect:(BOOL)shouldConvert {
    
    if ( shouldConvert ) {
        rect = [nav.topViewController.view convertRect:rect toView:nav.view];
    }
    
    return CGRectContainsPoint(rect, point);
}
@end


#pragma mark -

@interface YZSnapshot : NSObject
- (instancetype)initWithTarget:(UIViewController *)target;
@end

@interface YZSnapshot ()
@property (nonatomic, weak, readonly, nullable) UIViewController *target;
@property (nonatomic, strong, readonly) UIView *rootView;
@property (nonatomic, strong, nullable) UIView *maskView;
@end

@implementation YZSnapshot
- (instancetype)initWithTarget:(UIViewController *)target {
    self = [super init];
    if ( self ) {
        // target
        _target = target;
        
        // nav
        UINavigationController *nav = target.navigationController;
        _rootView = [[UIView alloc] initWithFrame:nav.view.bounds];
        _rootView.backgroundColor = UIColor.clearColor;
        
        // snapshot
        switch ( target.YZ_displayMode ) {
            case YZPreViewDisplayModeSnapshot: {
                UIView *superview = nav.tabBarController != nil ? nav.tabBarController.view : nav.view;
                UIView *snapshot = [superview snapshotViewAfterScreenUpdates:NO];
                [_rootView addSubview:snapshot];
            }
                break;
            case YZPreViewDisplayModeOrigin: {
                if ( nav.isNavigationBarHidden == false ) {
                    CGRect rect = [nav.view convertRect:nav.navigationBar.frame toView:nav.view.window];
                    rect.size.height += rect.origin.y + 1;
                    rect.origin.y = 0;
                    UIView *navbarSnapshot = [nav.view.superview resizableSnapshotViewFromRect:rect afterScreenUpdates:false withCapInsets:UIEdgeInsetsZero];
                    [_rootView addSubview:navbarSnapshot];
                }
                
                
                UITabBar *tabBar = nav.tabBarController.tabBar;
                if ( tabBar.isHidden == false ) {
                    CGRect rect = [tabBar convertRect:tabBar.bounds toView:nav.view.window];
                    rect.origin.y -= 1;
                    rect.size.height += 1;
                    UIView *snapshot = [nav.view.window resizableSnapshotViewFromRect:rect afterScreenUpdates:false withCapInsets:UIEdgeInsetsZero];
                    snapshot.frame = rect;
                    [_rootView addSubview:snapshot];
                }
            }
                break;
        }
        
        // mask
        if ( YZFullscreenPopGesture.transitionMode == YZFullscreenPopGestureTransitionModeMaskAndShifting ) {
            _maskView = [[UIView alloc] initWithFrame:_rootView.bounds];
            _maskView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.8];
            [_rootView addSubview:_maskView];
        }
    }
    return self;
}

- (void)began {
    if ( _target.YZ_displayMode == YZPreViewDisplayModeOrigin ) {
        [_rootView insertSubview:_target.view atIndex:0];
    }
}

- (void)completed {
    if ( _target.YZ_displayMode == YZPreViewDisplayModeOrigin &&
         _target.view.superview == _rootView ) {
        [_target.view removeFromSuperview];
    }
}

@end


#pragma mark -

@interface UIViewController (_YZFullscreenPopGesturePrivate)
@property (nonatomic, strong, nullable) YZSnapshot *YZ_previousViewControllerSnapshot;
@end

@implementation UIViewController (_YZFullscreenPopGesturePrivate)
- (void)setYZ_previousViewControllerSnapshot:(nullable YZSnapshot *)YZ_previousViewControllerSnapshot {
    objc_setAssociatedObject(self, @selector(YZ_previousViewControllerSnapshot), YZ_previousViewControllerSnapshot, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (nullable YZSnapshot *)YZ_previousViewControllerSnapshot {
    return objc_getAssociatedObject(self, _cmd);
}
@end


#pragma mark -
@interface YZTransitionBackgroundView : UIView
@end

@implementation YZTransitionBackgroundView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if ( self ) {
        self.backgroundColor = UIColor.whiteColor;
        self.layer.shadowOffset = CGSizeMake(0.5, 0);
        self.layer.shadowColor = [[UIColor alloc] initWithWhite:0.2 alpha:1].CGColor;
        self.layer.shadowOpacity = 1;
        self.layer.shadowRadius = 2;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
}
@end


@interface YZTransitionHandler : NSObject
+ (instancetype)shared;

@property (nonatomic) CGFloat shift;
@property (nonatomic, strong, readonly) YZTransitionBackgroundView *backgroundView;
@end

@implementation YZTransitionHandler
+ (instancetype)shared {
    static YZTransitionHandler *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = YZTransitionHandler.alloc.init;
    });
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _shift = - UIScreen.mainScreen.bounds.size.width * 0.382;
        _backgroundView = [YZTransitionBackgroundView.alloc initWithFrame:CGRectZero];
    }
    return self;
}

- (void)pushWithNav:(UINavigationController *)nav viewController:(UIViewController *)viewController {
    UIViewController *last = nav.childViewControllers.lastObject;
    if ( last != nil ) {
        viewController.YZ_previousViewControllerSnapshot = [YZSnapshot.alloc initWithTarget:last];
    }
}

- (void)beganWithNav:(UINavigationController *)nav viewController:(UIViewController *)viewController offset:(CGFloat)offset {
    YZSnapshot *snapshot = viewController.YZ_previousViewControllerSnapshot;
    if ( snapshot == nil )
        return;
    
    // keyboard
    [nav.view endEditing:YES];
    
    //
    [nav.view.superview insertSubview:snapshot.rootView belowSubview:nav.view];
    [nav.view insertSubview:_backgroundView atIndex:0];
    _backgroundView.frame = nav.view.bounds;
    
    //
    [snapshot began];
    
    snapshot.rootView.transform = CGAffineTransformMakeTranslation(self.shift, 0);
    
    if ( YZFullscreenPopGesture.transitionMode == YZFullscreenPopGestureTransitionModeMaskAndShifting ) {
        snapshot.maskView.alpha = 1;
        CGFloat width = snapshot.rootView.frame.size.width;
        snapshot.maskView.transform = CGAffineTransformMakeTranslation(-(self.shift + width), 0);
    }
    
    //
    if ( viewController.YZ_viewWillBeginDragging ) {
        viewController.YZ_viewWillBeginDragging(viewController);
    }
    
    [self changedWithNav:nav viewController:viewController offset:offset];
}

- (void)changedWithNav:(UINavigationController *)nav viewController:(UIViewController *)viewController offset:(CGFloat)offset {
    YZSnapshot *snapshot = viewController.YZ_previousViewControllerSnapshot;
    if ( snapshot == nil )
        return;

    if ( offset < 0 ) offset = 0;
    
    //
    nav.view.transform = CGAffineTransformMakeTranslation(offset, 0);
    
    //
    CGFloat width = snapshot.rootView.frame.size.width;
    CGFloat rate = offset / width;
    
    snapshot.rootView.transform = CGAffineTransformMakeTranslation(self.shift * ( 1 - rate), 0);
    
    if ( YZFullscreenPopGesture.transitionMode == YZFullscreenPopGestureTransitionModeMaskAndShifting ) {
        snapshot.maskView.alpha = 1 - rate;
        snapshot.maskView.transform = CGAffineTransformMakeTranslation(-(self.shift + width) + (self.shift *rate) + offset, 0);
    }
    
    //
    if ( viewController.YZ_viewDidDrag ) {
        viewController.YZ_viewDidDrag(viewController);
    }
}

- (void)completedWithNav:(UINavigationController *)nav viewController:(UIViewController *)viewController offset:(CGFloat)offset {
    YZSnapshot *snapshot = viewController.YZ_previousViewControllerSnapshot;
    if ( snapshot == nil )
        return;
    
    CGFloat screenwidth = nav.view.frame.size.width;
    CGFloat rate = offset / screenwidth;
    CGFloat maxOffset = YZFullscreenPopGesture.maxOffsetToTriggerPop;
    BOOL shouldPop = rate > maxOffset;
    CGFloat animDuration = 0.25;
    
    if ( shouldPop == false ) {
        animDuration = animDuration * ( offset / (maxOffset * screenwidth) ) + 0.05;
    }
    
    [UIView animateWithDuration:animDuration animations:^{
        if ( shouldPop == true ) {
            snapshot.rootView.transform = CGAffineTransformIdentity;
            snapshot.maskView.transform = CGAffineTransformIdentity;
            snapshot.maskView.alpha = 0.001;
            
            nav.view.transform = CGAffineTransformMakeTranslation(screenwidth, 0);
        }
        else {
            snapshot.maskView.transform = CGAffineTransformMakeTranslation(-(self.shift + screenwidth), 0);
            snapshot.maskView.alpha = 1;
            
            nav.view.transform = CGAffineTransformIdentity;
        }
    } completion:^(BOOL finished) {
        [self.backgroundView removeFromSuperview];
        [snapshot.rootView removeFromSuperview];
        [snapshot completed];
        
        if ( shouldPop == true ) {
            nav.view.transform = CGAffineTransformIdentity;
            [nav popViewControllerAnimated:false];
        }
        
        if ( viewController.YZ_viewDidEndDragging ) {
            viewController.YZ_viewDidEndDragging(viewController);
        }
    }];
}
@end


#pragma mark -

@interface UINavigationController (_YZFullscreenPopGesturePrivate)
@property (nonatomic, strong, readonly) UIPanGestureRecognizer *YZ_fullscreenGesture;
@end

@implementation UINavigationController (_YZFullscreenPopGesturePrivate)
+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class cls = UINavigationController.class;
        SEL originalSelector = @selector(pushViewController:animated:);
        SEL swizzledSelector = @selector(YZ_pushViewController:animated:);
        
        Method originalMethod = class_getInstanceMethod(cls, originalSelector);
        Method swizzledMethod = class_getInstanceMethod(cls, swizzledSelector);
        method_exchangeImplementations(originalMethod, swizzledMethod);
    });
}

- (void)YZ_pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [self YZ_setupIfNeeded];
    [YZTransitionHandler.shared pushWithNav:self viewController:viewController];
    [self YZ_pushViewController:viewController animated:animated];
}

- (void)YZ_setupIfNeeded {
    if ( self.interactivePopGestureRecognizer == nil )
        return;
    
    if ( [objc_getAssociatedObject(self, _cmd) boolValue] )
        return;
    
    objc_setAssociatedObject(self, _cmd, @(YES), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    self.interactivePopGestureRecognizer.enabled = false;
    self.view.clipsToBounds = false;
    
    [self.view addGestureRecognizer:self.YZ_fullscreenGesture];
}

- (UIPanGestureRecognizer *)YZ_fullscreenGesture {
    UIPanGestureRecognizer *_Nullable gesture = objc_getAssociatedObject(self, _cmd);
    if ( gesture == nil ) {
        if ( YZFullscreenPopGesture.gestureType == YZFullscreenPopGestureTypeEdgeLeft ) {
            gesture = UIScreenEdgePanGestureRecognizer.alloc.init;
            [(UIScreenEdgePanGestureRecognizer *)gesture setEdges:UIRectEdgeLeft];
        }
        else {
            gesture = UIPanGestureRecognizer.alloc.init;
        }
        
        gesture.delaysTouchesBegan = YES;
        gesture.delegate = YZFullscreenPopGestureDelegate.shared;
        [gesture addTarget:self action:@selector(YZ_handleFullscreenGesture:)];
        objc_setAssociatedObject(self, _cmd, gesture, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return gesture;
}

- (void)YZ_handleFullscreenGesture:(UIPanGestureRecognizer *)gesture {
    CGFloat offset = [gesture translationInView:gesture.view].x;
    switch ( gesture.state ) {
        case UIGestureRecognizerStateBegan:
            [YZTransitionHandler.shared beganWithNav:self viewController:self.topViewController offset:offset];
            break;
        case UIGestureRecognizerStateChanged:
            [YZTransitionHandler.shared changedWithNav:self viewController:self.topViewController offset:offset];
            break;
        case UIGestureRecognizerStateEnded: case UIGestureRecognizerStateCancelled: case UIGestureRecognizerStateFailed:
            [YZTransitionHandler.shared completedWithNav:self viewController:self.topViewController offset:offset];
            break;
        case UIGestureRecognizerStatePossible:
            break;
    }
}
@end


#pragma mark -

@implementation YZFullscreenPopGesture
static YZFullscreenPopGestureType _gestureType = YZFullscreenPopGestureTypeEdgeLeft;
+ (void)setGestureType:(YZFullscreenPopGestureType)gestureType {
    _gestureType = gestureType;
}
+ (YZFullscreenPopGestureType)gestureType {
    return _gestureType;
}

static YZFullscreenPopGestureTransitionMode _transitionMode = YZFullscreenPopGestureTransitionModeShifting;
+ (void)setTransitionMode:(YZFullscreenPopGestureTransitionMode)transitionMode {
    _transitionMode = transitionMode;
}
+ (YZFullscreenPopGestureTransitionMode)transitionMode {
    return _transitionMode;
}

static CGFloat _maxOffsetToTriggerPop = 0.35;
+ (void)setMaxOffsetToTriggerPop:(CGFloat)maxOffsetToTriggerPop {
    _maxOffsetToTriggerPop = maxOffsetToTriggerPop;
}
+ (CGFloat)maxOffsetToTriggerPop {
    return _maxOffsetToTriggerPop;
}
@end


#pragma mark -

@implementation UIViewController (YZExtendedFullscreenPopGesture)
- (void)setYZ_displayMode:(YZPreViewDisplayMode)YZ_displayMode {
    self.edgesForExtendedLayout = UIRectEdgeNone;
    objc_setAssociatedObject(self, @selector(YZ_displayMode), @(YZ_displayMode), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (YZPreViewDisplayMode)YZ_displayMode {
    return [objc_getAssociatedObject(self, _cmd) integerValue];
}

- (void)setYZ_disableFullscreenGesture:(BOOL)YZ_disableFullscreenGesture {
    objc_setAssociatedObject(self, @selector(YZ_disableFullscreenGesture), @(YZ_disableFullscreenGesture), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (BOOL)YZ_disableFullscreenGesture {
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

- (void)setYZ_blindArea:(nullable NSArray<NSValue *> *)YZ_blindArea {
    objc_setAssociatedObject(self, @selector(YZ_blindArea), YZ_blindArea, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (nullable NSArray<NSValue *> *)YZ_blindArea {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setYZ_blindAreaViews:(nullable NSArray<UIView *> *)YZ_blindAreaViews {
    objc_setAssociatedObject(self, @selector(YZ_blindAreaViews), YZ_blindAreaViews, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (nullable NSArray<UIView *> *)YZ_blindAreaViews {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setYZ_viewWillBeginDragging:(void (^_Nullable)(__kindof UIViewController * _Nonnull))YZ_viewWillBeginDragging {
    objc_setAssociatedObject(self, @selector(YZ_viewWillBeginDragging), YZ_viewWillBeginDragging, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (void (^_Nullable)(__kindof UIViewController * _Nonnull))YZ_viewWillBeginDragging {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setYZ_viewDidDrag:(void (^_Nullable)(__kindof UIViewController * _Nonnull))YZ_viewDidDrag {
    objc_setAssociatedObject(self, @selector(YZ_viewDidDrag), YZ_viewDidDrag, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (void (^_Nullable)(__kindof UIViewController * _Nonnull))YZ_viewDidDrag {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setYZ_viewDidEndDragging:(void (^_Nullable)(__kindof UIViewController * _Nonnull))YZ_viewDidEndDragging {
    objc_setAssociatedObject(self, @selector(YZ_viewDidEndDragging), YZ_viewDidEndDragging, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (void (^_Nullable)(__kindof UIViewController * _Nonnull))YZ_viewDidEndDragging {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setYZ_considerWebView:(nullable WKWebView *)YZ_considerWebView {
    YZ_considerWebView.allowsBackForwardNavigationGestures = YES;
    objc_setAssociatedObject(self, @selector(YZ_considerWebView), YZ_considerWebView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (nullable WKWebView *)YZ_considerWebView {
    return objc_getAssociatedObject(self, _cmd);
}

@end

#pragma mark -
@implementation UINavigationController (YZExtendedFullscreenPopGesture)
- (UIGestureRecognizerState)YZ_fullscreenGestureState {
    return self.YZ_fullscreenGesture.state;
}
- (void)YZ_popViewController:(BOOL)animated {
    [UIView animateWithDuration:0.3 animations:^{
        [YZTransitionHandler.shared beganWithNav:self viewController:self.topViewController offset:self.view.frame.size.width];
    }completion:^(BOOL finished) {
        [YZTransitionHandler.shared completedWithNav:self viewController:self.topViewController offset:self.view.frame.size.width];
    }];
}
@end
NS_ASSUME_NONNULL_END
