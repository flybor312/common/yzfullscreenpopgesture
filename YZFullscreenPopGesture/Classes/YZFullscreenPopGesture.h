//
//  YZFullscreenPopGesture.h
//  YZBackGRProject
//
//  Created by  on 2019/7/17.
//  Copyright © 2019 . All rights reserved.
//

#import <UIKit/UIKit.h>
@class WKWebView;

NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    YZFullscreenPopGestureTypeEdgeLeft,
    YZFullscreenPopGestureTypeFull,
} YZFullscreenPopGestureType;

typedef enum : NSUInteger {
    YZFullscreenPopGestureTransitionModeShifting,
    YZFullscreenPopGestureTransitionModeMaskAndShifting,
} YZFullscreenPopGestureTransitionMode;

typedef enum : NSUInteger {
    YZPreViewDisplayModeSnapshot,
    YZPreViewDisplayModeOrigin,
} YZPreViewDisplayMode;

@interface YZFullscreenPopGesture : NSObject
@property (class, nonatomic) YZFullscreenPopGestureType gestureType;
@property (class, nonatomic) YZFullscreenPopGestureTransitionMode transitionMode;
@property (class, nonatomic) CGFloat maxOffsetToTriggerPop;
@end

@interface UIViewController (YZExtendedFullscreenPopGesture)
@property (nonatomic) YZPreViewDisplayMode YZ_displayMode;
@property (nonatomic) BOOL YZ_disableFullscreenGesture;
@property (nonatomic, copy, nullable) NSArray<NSValue *> *YZ_blindArea;
@property (nonatomic, copy, nullable) NSArray<UIView *> *YZ_blindAreaViews;

@property (nonatomic, copy, nullable) void(^YZ_viewWillBeginDragging)(__kindof UIViewController *vc);
@property (nonatomic, copy, nullable) void(^YZ_viewDidDrag)(__kindof UIViewController *vc);
@property (nonatomic, copy, nullable) void(^YZ_viewDidEndDragging)(__kindof UIViewController *vc);

@property (nonatomic, strong, nullable) WKWebView *YZ_considerWebView;
@end

@interface UINavigationController (YZExtendedFullscreenPopGesture)
@property (nonatomic, readonly) UIGestureRecognizerState YZ_fullscreenGestureState;
- (void)YZ_popViewController:(BOOL)animated;

@end
NS_ASSUME_NONNULL_END
