#
# Be sure to run `pod lib lint YZFullscreenPopGesture.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YZFullscreenPopGesture'
  s.version          = '0.1.0'
  s.summary          = 'A short description of YZFullscreenPopGesture.'


  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/flybor312/common/yzfullscreenpopgesture'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '510462142@qq.com' => '18780106592@163.com' }
  s.source           = { :git => 'https://gitlab.com/flybor312/common/yzfullscreenpopgesture.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'YZFullscreenPopGesture/Classes/**/*'
  
  # s.resource_bundles = {
  #   'YZFullscreenPopGesture' => ['YZFullscreenPopGesture/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
