# YZFullscreenPopGesture

[![CI Status](https://img.shields.io/travis/510462142@qq.com/YZFullscreenPopGesture.svg?style=flat)](https://travis-ci.org/510462142@qq.com/YZFullscreenPopGesture)
[![Version](https://img.shields.io/cocoapods/v/YZFullscreenPopGesture.svg?style=flat)](https://cocoapods.org/pods/YZFullscreenPopGesture)
[![License](https://img.shields.io/cocoapods/l/YZFullscreenPopGesture.svg?style=flat)](https://cocoapods.org/pods/YZFullscreenPopGesture)
[![Platform](https://img.shields.io/cocoapods/p/YZFullscreenPopGesture.svg?style=flat)](https://cocoapods.org/pods/YZFullscreenPopGesture)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YZFullscreenPopGesture is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'YZFullscreenPopGesture'
```

## Author

510462142@qq.com, 18780106592@163.com

## License

YZFullscreenPopGesture is available under the MIT license. See the LICENSE file for more info.
