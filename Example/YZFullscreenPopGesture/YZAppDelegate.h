//
//  YZAppDelegate.h
//  YZFullscreenPopGesture
//
//  Created by 510462142@qq.com on 02/03/2021.
//  Copyright (c) 2021 510462142@qq.com. All rights reserved.
//

@import UIKit;

@interface YZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
