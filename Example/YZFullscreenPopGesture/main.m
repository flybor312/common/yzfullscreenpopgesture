//
//  main.m
//  YZFullscreenPopGesture
//
//  Created by 510462142@qq.com on 02/03/2021.
//  Copyright (c) 2021 510462142@qq.com. All rights reserved.
//

@import UIKit;
#import "YZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YZAppDelegate class]));
    }
}
